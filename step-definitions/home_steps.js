module.exports = function() {    
    this.Given(/^I logged in as Admin$/, function() {
        helpers.loadPage(shared.testData.loginUrl).then(function(){
            return page.loginPage.loginToAccount('admin@gmail.com', 'admin');
        });        
    });
    
    this.When(/^I upload incorrect upwork-report$/, function() {
        return page.homePage.uploadUpworkReport(shared.testData.upwReport);
    });
    
    this.Then(/^I see message about report$/, function () {
        return page.loginPage.checkTitle(page.homePage.elements.errorHeader, shared.testData.errorText);
    });
}
