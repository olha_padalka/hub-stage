module.exports = function () {    
    this.Given(/^I am on login page$/, function () {
        return helpers.loadPage(shared.testData.loginUrl);
    });

    this.When(/^I fill in (.*) and (.*) fields$/, function (email, password) { 
        return page.loginPage.loginToAccount(email, password);
    });

    this.Then(/^I see home (.*)$/, function (widget) {
        return page.loginPage.checkTitle(page.homePage.elements.userTitle, widget + ' ');
    });

    this.When(/^I enter wrong (.*) or wrong (.*)$/, function (email, password) { 
        return page.loginPage.loginToAccount(email, password);
    });

    this.Then(/^I see error (.*)$/, function (message) {
        return page.loginPage.checkTitle(page.loginPage.elements.errorBadge, message);
    });    
};