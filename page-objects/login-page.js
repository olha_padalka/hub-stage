module.exports = {
    elements: {
        emailField: 'email',
        passwordField: 'password',
        loginBtn: '.btn',
        errorBadge: 'strong',
    }, 
    loginToAccount: function(email, password) {
        driver.wait(until.elementLocated(by.id(page.loginPage.elements.emailField))).sendKeys(email);
        driver.wait(until.elementLocated(by.id(page.loginPage.elements.passwordField))).sendKeys(password);
        return driver.findElement(by.css(page.loginPage.elements.loginBtn)).click();
    },
    checkTitle: function(titleCss, titleName){
        return driver.wait(until.elementsLocated(by.css(titleCss))).then(function () {
            return helpers.getAttributeValue(titleCss, 'innerText');
        }).then((element) => {
            expect(element).to.equal(titleName);
        });
    }
};