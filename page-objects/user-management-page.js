module.exports = {
    elements: {
        addBtn: '[data-toggle=modal]',
        inputEmail: '#emailInvite',
        sendInviteBtn: '[type=submit]'
    }
};
