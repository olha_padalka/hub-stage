module.exports = { 
    homePageURL: 'https://hub-staging.clockwise.software/home',
    elements: {
        userTitle: '.dropdown-toggle',
        tahBtn: 'tahometer_button_file',
        file_input: 'input[type="file"]',
        spanDate: '.padding-right-0px > div:nth-child(1)',
        upworkBtn: '[data-target="#upworkUpdate"]', 
        input_upwork: 'textarea[placeholder="Paste Upwork report"]', 
        updateBtn: '[type="submit"]',
        spanDate2: '.padding-right-0px > div:nth-child(2)',
        errorHeader: 'body > div.sweet-alert.showSweetAlert.visible > h2', 
    },
    uploadTahReport: (fileCsv) => {
        driver.wait(until.elementLocated(by.id(page.homePage.elements.tahBtn))).click()
        .then(function(){
            return driver.wait(until.elementLocated(by.css(page.homePage.elements.file_input))).sendKeys(fileCsv);
        });  
    },
    uploadUpworkReport: (f_txt) => {
        driver.wait(until.elementLocated(by.css(page.homePage.elements.upworkBtn))).click()
        driver.wait(until.elementLocated(by.css(page.homePage.elements.input_upwork)), 5000)
        driver.wait(until.elementIsVisible(driver.findElement(By.css(page.homePage.elements.input_upwork)), 5000)).sendKeys(f_txt);
        return driver.wait(until.elementLocated(by.css(page.homePage.elements.updateBtn))).click();        
    }
}