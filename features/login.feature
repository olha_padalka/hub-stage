Feature: Logging in cw-hub

@test
Scenario Outline: Login with Admin/User/HR credentials 
Given I am on login page
When I fill in <email> and <password> fields 
Then I see home <widget>

Examples: Correct credentials
|email|password|widget|
|admin@gmail.com|admin|Admin|
|nightfoxolya@gmail.com|password|Olha|
|nightfoxolya+hr@gmail.com|password|Maria|

Scenario Outline: Login with invalid credentials
Given I am on login page
When I enter wrong <email> or wrong <password>
Then I see error <message>

Examples: Wrong passwords or wrong email:
|email|password|message|
|admin@gmail.com|admin###|These credentials do not match our records.|
|adm#n@gmail.com|admin|These credentials do not match our records.|
| | |The email field is required.|
|nightfoxolya@gmail.com|passw|These credentials do not match our records.|
|nightfoxolya@gmail.com|8TFyJIOYd9GoJlqKrWepwpesNi1B8G5Oy9lykMTtnxjzXQy0dlI|These credentials do not match our records.|