Feature: Upload Reports

Scenario: Upload incorrect Upwork report
Given I logged in as Admin
When I upload incorrect upwork-report
Then I see message about report

Scenario: Upload tahometer report csv
Given I logged in as Admin
When I load tahometer file csv
Then I see success pop-up message

Scenario: Upload wrong file format for tahometer 
Given I logged in as Admin
When I load incorrect file format
Then I see error-message about it
